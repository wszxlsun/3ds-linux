#include <linux/gpio_keys.h>
#include <linux/platform_device.h>
#include <linux/input.h>
#include <linux/module.h>
#include <linux/dmi.h>
#include <linux/of_gpio.h>

#define DRIVER_NAME "3ds-gpio-keys"
#define KEY_BUTTON(_code, _type) \
    { \
		.type	= EV_KEY, \
		.code	= _code, \
		.gpio	= 0, \
        .active_low = 1, \
		.desc	= "3ds button " #_code, \
		.debounce_interval = 200, \
	}
static char *gpio_key_names[] = {
    "a",
    "b",
    "select",
    "start",
    "right",
    "left",
    "up",
    "down",
    "trig_right",
    "trig_left",
    "x",
    "y"
};

static struct gpio_keys_button gpio_keys[] = {
		KEY_BUTTON(KEY_ENTER, EV_KEY),
        KEY_BUTTON(KEY_BACKSPACE, EV_KEY),
        KEY_BUTTON(KEY_TAB, EV_KEY),
        KEY_BUTTON(KEY_ESC, EV_KEY),
        KEY_BUTTON(KEY_RIGHT, EV_KEY),
        KEY_BUTTON(KEY_LEFT, EV_KEY),
        KEY_BUTTON(KEY_UP, EV_KEY),
        KEY_BUTTON(KEY_DOWN, EV_KEY),
        KEY_BUTTON(KEY_MENU, EV_KEY),
        KEY_BUTTON(KEY_POWER, EV_KEY),
        KEY_BUTTON(KEY_HOME, EV_KEY),
        KEY_BUTTON(KEY_END, EV_KEY),
};
static struct gpio_keys_platform_data gpio_keys_data = {
	.buttons = gpio_keys,
	.nbuttons = ARRAY_SIZE(gpio_keys),
    .poll_interval	= 60,
};

static struct platform_device gpio_polled_keys = {
	.name = "gpio-keys-polled",
    .id = -1,
	.dev = {
		.platform_data = &gpio_keys_data,
	},
};

static struct platform_device *gpio_polled_keys_dev[] __initdata = {
	&gpio_polled_keys
};
static int register_keys(void)
{
    int ret = platform_add_devices(gpio_polled_keys_dev, ARRAY_SIZE(gpio_polled_keys_dev));
    if (ret != 0) 
        pr_err(DRIVER_NAME "add_devices failed!");
    return ret;
}

inline int probe_gpio_keys(void){
    struct device_node *np;
	int i, gpio_no;

	if (!of_machine_is_compatible("nintendo,3ds"))
    {
	    printk(KERN_ERR DRIVER_NAME ": device tree no match %s\n", "nintendo,3ds");
    	return -ENODEV;
    }
	np = of_find_node_by_name(NULL, "hid_buttons");
	if (!np)
    {
        printk(KERN_ERR DRIVER_NAME ": can not find node: %s in device tree %s\n", "hid_buttons", "nintendo,3ds");
		return -ENODEV;
    }
	for (i = 0; i < ARRAY_SIZE(gpio_keys); i++) {
        np = of_find_node_by_name(np, gpio_key_names[i]);
		gpio_no = of_get_gpio(np, 0);
		if (gpio_no< 0) {
            printk(KERN_ERR DRIVER_NAME ": get gpio number of %s failed!\n", gpio_keys[i].desc);
			return -ENODEV;
		}
        printk(KERN_INFO DRIVER_NAME ": key: %s gpio: %d\n", gpio_keys[i].desc, gpio_no);
        gpio_keys[i].gpio = gpio_no;
    }
    return 0;
}

static int __init gpio_polled_keys_init(void)
{
    int ret;
    printk(KERN_INFO "%s: map gpio keys\n", DRIVER_NAME);
    ret = probe_gpio_keys();
    if (ret != 0) return ret;
    ret = register_keys();
    if (ret != 0) return ret;
	return 0;
}
device_initcall(gpio_polled_keys_init);